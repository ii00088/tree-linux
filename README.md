# Linux Tree CLI
## Usage
```
$ make build
$ make run directory=tests filter=2021-04-22 #Runs the command using filter and prints in terminal.
$ make json directory=tests #Runs the CLI using -j to output to JSON.
```

## Testing
```
$ make test
```
## Discussion
The application runs in 2 different modes: Terminal and JSON. Terminal outputs the tree structure. JSON outputs the structure to a JSON file.

## Dependencies
- You'd need to have GOPATH exported.  GOBIN for MACOS for go get command