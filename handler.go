package main

import (
	"fmt"
	"os"
	"path"
	"sort"
	"time"
)

//Counter strucutre need for presenting final results
type Counter struct {
	files       int
	directories int
}

//Function that checks if path is directory of folder
func (counter *Counter) isDir(path string) {
	stat, _ := os.Stat(path)
	if stat.IsDir() {
		counter.directories += 1
	} else {
		counter.files += 1
	}
}

//Function for opening a directory returns all the names of the directories as well as their indices.
func opendirectory(base string) []string {
	file, err := os.Open(base)
	if err != nil {
		ErrorLogger.Println(err)
	}
	//No need to catch the error as it might be a file not a directory.
	dirnames, _ := file.Readdirnames(0)

	defer file.Close()

	sort.Strings(dirnames)
	return dirnames
}

//Recursive function called on each directory. The function appends each path found
//to a subpath called next, checks if each path is directory or file and appends to counter.
//A print statment is based on the index of the path in the current directory.
func tree(prefix string, counter *Counter, base string, t time.Time) {
	dirnames := opendirectory(base)

	for index, name := range dirnames {
		if name[0] == '.' {
			continue
		}

		next := path.Join(base, name)
		stat, _ := os.Stat(next)
		if stat.ModTime().Before(t) {
			continue
		}
		counter.isDir(next)
		if index < len(dirnames)-1 {
			fmt.Println(prefix+"├──", name)
			tree(prefix+"│   ", counter, next, t)
		} else {
			fmt.Println(prefix+"└──", name)
			tree(prefix+"    ", counter, next, t)
		}
	}
}
