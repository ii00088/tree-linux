package main

import (
	"fmt"
	"testing"
	"time"
)

/*
Standard assert Equals function
*/
func assertEqual(t *testing.T, a interface{}, b interface{}, message string) {
	if a == b {
		return
	}
	if len(message) == 0 {
		message = fmt.Sprintf("%v != %v", a, b)
	}
	t.Fatal(message)
}

/*
Test isDir() simple.
*/
func TestIsDir(t *testing.T) {
	counter := new(Counter)
	counter.isDir("./tests")
	counter.isDir("./tests/testfile")

	assertEqual(t, counter.directories, 1, "")
	assertEqual(t, counter.files, 1, "")
}

/*
Test isDir() simple.
*/
func TestIsDir2(t *testing.T) {
	counter := new(Counter)

	counter.isDir("./tests")
	counter.isDir("./tests/testFolder1")
	counter.isDir("./tests/testFolder2")

	counter.isDir("./tests/testfile")
	counter.isDir("./tests/testfile1")
	counter.isDir("./tests/testfile2")

	assertEqual(t, counter.directories, 3, "")
	assertEqual(t, counter.files, 3, "")
}

/*
Test openDirectory() for tests directory. Should return 5 paths
*/
func TestOpenDirectory(t *testing.T) {
	result := opendirectory("./tests")
	assertEqual(t, len(result), 5, "")
	assertEqual(t, result[0], "testFolder1", "")
	assertEqual(t, result[1], "testFolder2", "")
}

/*
Test if the tree function works by observing the counter.
*/
func TestTree(t *testing.T) {
	counter := new(Counter)
	filter := "2021-04-02"
	format = "2006-01-02"

	time, err := time.Parse(format, filter)
	if err != nil {
		ErrorLogger.Print("Format:", format)
		ErrorLogger.Print(err)
		return
	}
	tree("", counter, ".", time)
	assertEqual(t, counter.directories, 3, "")
	assertEqual(t, counter.files, 14, "")
}

/*
Test if the filter works on tree() by observing the counter.
*/
func TestFilter(t *testing.T) {
	counter := new(Counter)
	filter := "2021-04-23"
	format = "2006-01-02"

	time, err := time.Parse(format, filter)
	if err != nil {
		ErrorLogger.Print("Format:", format)
		ErrorLogger.Print(err)
		return
	}

	tree("", counter, ".", time)
	assertEqual(t, counter.directories, 0, "")
	assertEqual(t, counter.files, 9, "")
}
