package main

import (
	"os"
	"path"
	"strings"
)

//Recursive function called on each directory. The function checks if the
//current directory is Folder. If so each directory gets recursive call and
//gets appended to the Folder structure. If not the files are appended.
func jsontree(output *Folder, counter *Counter, base string) {
	dirnames := opendirectory(base)

	for _, name := range dirnames {
		//Exclude hiddenn folders
		if name[0] == '.' {
			continue
		}

		next := path.Join(base, name)
		counter.isDir(next)

		stat, _ := os.Stat(next)
		if stat.IsDir() {
			for i, x := range strings.Split(next, "/") {
				if i == len(strings.Split(next, "/"))-1 {
					output.Folders[x] = newFolder(x)
					jsontree(output.Folders[x], counter, next)
				}
			}
		} else {
			output.Files = append(output.Files, File{Name: name})
			jsontree(output, counter, next)
		}

	}
}
