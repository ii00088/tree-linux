package main

import (
	"testing"
)

/*
Test if new folders are created.
*/
func TestNewFolder(t *testing.T) {
	folder := newFolder("test")
	folder.Folders["test"] = newFolder("test1")
	assertEqual(t, folder.Name, "test", "")
	assertEqual(t, folder.Folders["test"].Name, "test1", "")
}

/*
Thoroughly testing JSON tree function.
*/
func TestJsonTree(t *testing.T) {
	folder := newFolder(".")
	counter := new(Counter)

	jsontree(folder, counter, ".")

	assertEqual(t, folder.Name, ".", "")

	assertEqual(t, len(folder.Folders), 1, "")
	assertEqual(t, len(folder.Files), 10, "")

	assertEqual(t, len(folder.Folders["tests"].Folders), 2, "")
	assertEqual(t, len(folder.Folders["tests"].Files), 3, "")

	assertEqual(t, len(folder.Folders["tests"].Folders["testFolder1"].Files), 1, "")
}
