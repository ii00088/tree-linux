package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)

//File structure representation
type File struct {
	Name string
}

//Folder structure representation
type Folder struct {
	Name    string
	Files   []File
	Folders map[string]*Folder
}

// New folder creation for Folder struction
func newFolder(name string) *Folder {
	return &Folder{name, []File{}, make(map[string]*Folder)}
}

//Declaring all the variables needed
var (
	ErrorLogger *log.Logger
	directory   string
	filter      string
	format      string
)

func main() {
	ErrorLogger = log.New(os.Stderr, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
	//Parse the flag and check if present.
	boolPtr := flag.Bool("j", false, "Output as JSON file")
	flag.Parse()
	if *boolPtr {
		//Start JSON routine.
		directory = "."
		if len(os.Args) > 2 {
			directory = os.Args[2]
		}
		output := newFolder(directory)
		counter := new(Counter)
		//Start recursive json tree.
		jsontree(output, counter, directory)
		jsonString, err := json.MarshalIndent(output, "", "    ")
		if err != nil {
			ErrorLogger.Fatal(err)
		}
		_ = ioutil.WriteFile("output.json", jsonString, 0644)
		fmt.Printf("%d directories, %d files\n", counter.directories, counter.files)
	} else {
		directory = "."
		format = "2006-01-02"

		if len(os.Args) > 2 {
			directory = os.Args[1]
			filter = os.Args[2]
		}

		t, err := time.Parse(format, filter)
		if err != nil {
			ErrorLogger.Print("Format:", format)
			ErrorLogger.Print(err)
			return
		}
		counter := new(Counter)
		//Start regular recursive tree.
		tree("", counter, directory, t)
		fmt.Printf("%d directories, %d files\n", counter.directories, counter.files)
	}

}
